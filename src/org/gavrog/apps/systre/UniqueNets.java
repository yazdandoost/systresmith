package org.gavrog.apps.systre;

import org.gavrog.box.collections.IteratorAdapter;
import org.gavrog.box.collections.Pair;
import org.gavrog.jane.numbers.IArithmetic;
import org.gavrog.jane.numbers.Real;
import org.gavrog.joss.geometry.CrystalSystem;
import org.gavrog.joss.geometry.Point;
import org.gavrog.joss.geometry.SpaceGroup;
import org.gavrog.joss.geometry.SpaceGroupFinder;

import org.gavrog.joss.pgraphs.basic.*;
import org.gavrog.joss.pgraphs.io.Net;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by Maryam on 12/29/16.
 */
public class UniqueNets {
    // =======================
// Helper Function
// =======================
    private static Pair<Net, HashMap<INode, HashSet<String>>> minimalImageWithNodeNames(Net net) {
        if (net.isMinimal()) {
            HashMap<INode, HashSet<String>> dict = new HashMap();
            for (INode node : net.nodes()) {
                HashSet<String> one = new HashSet<String> ();
                one.add(net.getNodeName(node));
                dict.put(node, one);
            }
            return new Pair<Net, HashMap<INode, HashSet<String>>>(net, dict);
        } else {
            Morphism phi = net.minimalImageMap();
            PeriodicGraph imageGraph = phi.getImageGraph();
            HashMap<INode, HashSet<String>> names = new HashMap();
            for (INode v : net.nodes()) {
                if (names.get(phi.getImage(v)).equals(null)){
                    names.put(phi.getImage(v), new HashSet<>());
                }
                names.get(phi.getImage(v)).add(net.getNodeName(v));
            }
            HashMap<INode, HashSet<String>> nodeToNames = new HashMap();


            for(INode node: imageGraph.nodes()){
                List<String> list = new ArrayList(names.get(node));
                Collections.sort(list);
                HashSet<String> sortedSet = new HashSet(list);
                nodeToNames.put(node, sortedSet);
            }
            return new Pair(imageGraph, nodeToNames);
        }
    }

// =======================
// Main Data Processing
// =======================

// --- dictionary of seen nets

    // --- count the nets that we read
    public static void main(String[] args) {
        int count = 0;

        try {
            Net G0 = Net.extractNet(args[0]);

            String name = G0.getName();
            if (name == null) {
                name = "unamed-" + String.format("%03d", count);
            }
            System.out.println("Net " + String.format("%03d", count) + " (" + name + "): ");


            Pair<Net, HashMap<INode, HashSet<String>>> pair = minimalImageWithNodeNames(G0);


            // print node names, possibly composites from original names
            System.out.println("\tNodes:");
            IteratorAdapter<INode> nodes = ((Net) pair.getFirst()).nodes();
            while (nodes.hasNext()) {
                INode v = nodes.next();
                HashMap<INode, HashSet<String>> nodeToNames = pair.getSecond();
                Iterator join = nodeToNames.get(v).iterator();
                System.out.print("\t\t " + v.id() + " ");
                while (join.hasNext()) {
                    System.out.println(join.next() + " ");
                }
            }

            // print the name of the space group
            SpaceGroup group = ((Net) pair.getFirst()).getSpaceGroup();
            System.out.println("\tGroup: \t" + group.getName());

            // print out the graph expressed in terms of a conventional unit cell
            Net G = pair.getFirst();
            Cover cover = G.conventionalCellCover();
            Map<INode, Point> coverPos = cover.barycentricPlacement();
            SpaceGroupFinder finder = new SpaceGroupFinder(group);
            CrystalSystem system = finder.getCrystalSystem();
            boolean hasSpecialAngle = ((system == CrystalSystem.TRIGONAL) ||
                    (system == CrystalSystem.HEXAGONAL_2D) || (system == CrystalSystem.HEXAGONAL_3D));
            System.out.println("\tUnit cell parameters:");
            System.out.print("\t\t1 1 1 90 90 ");
            if (hasSpecialAngle) {
                System.out.println("120");
            } else {
                System.out.println("90");
            }
            System.out.println("\tUnit cell positions:");

            IteratorAdapter<INode> coverNodes = cover.nodes();
            for (INode v : coverNodes) {
                System.out.print("\t\t" + v.id() + " ");
                Point p = coverPos.get(v);
                for (int i = 0; i < p.getDimension(); i++) {
                    IArithmetic ari = p.get(i); // TODO: will find how to get good number of digits

                    if (ari instanceof Real) {
                        double v1 = ((Real) ari).doubleValue();
                        System.out.print(Double.valueOf(v1) + " ");
                    }
                }
                System.out.println("\t" + cover.getNodeIdToName().get(v.id()));

                System.out.println();
            }
            System.out.println("\tUnit cell edges:");
            for (IEdge e : cover.edges()) {
                INode source = e.source();
                INode target = e.target();
                org.gavrog.joss.geometry.Vector s = cover.getShift(e);
                System.out.print("\t\t" + source.id() + " " + target.id() + "  ");
                for (int j = 0; j < s.getDimension(); j++) {
                    IArithmetic arithmetic = s.get(j);
                    String repr = arithmetic.toString();
                    String str = repr.substring(0, Math.min(2, repr.length()));
                    System.out.print(str + " "); //TODO: format to be two characters
                }
                System.out.println();

            }

            System.out.println();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
}