package org.gavrog.apps.systre;

import org.gavrog.joss.pgraphs.io.Net;

import java.io.FileNotFoundException;

/**
 * Created by rebecca on 11/19/16.
 */
public class UniqueNetsE {

// =======================
// Main Data Processing
// =======================

// --- dictionary of seen nets

    // --- count the nets that we read
    public static void main(String[] args) {
        try {
            Net G0 = Net.extractNet(args[0]);
            System.out.println(G0);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}