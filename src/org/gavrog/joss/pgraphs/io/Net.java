/*
   Copyright 2013 Olaf Delgado-Friedrichs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package org.gavrog.joss.pgraphs.io;

import org.gavrog.box.collections.*;
import org.gavrog.box.simple.Tag;
import org.gavrog.jane.compounds.Matrix;
import org.gavrog.jane.numbers.IArithmetic;
import org.gavrog.joss.dsyms.basic.DSymbol;
import org.gavrog.joss.dsyms.generators.InputIterator;
import org.gavrog.joss.geometry.*;
import org.gavrog.joss.geometry.Vector;
import org.gavrog.joss.pgraphs.basic.*;
import org.gavrog.joss.tilings.FaceList;
import org.gavrog.joss.tilings.Tiling;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

/**
 * Encapsulates a net with extra information picked up by the parser.
 */
public class Net extends PeriodicGraph {
    final private String name;
    final private String givenGroup;
    final private Map<INode, String> nodeToName = new HashMap<>();
    final private Map<Pair<INode, Object>, Object> nodeInfo = new HashMap<>();
    private Boolean isOk;
    private Matrix cellGramMatrixOriginal;
    private Matrix cellGramMatrix;
    private Map<INode, Point> nodeToPosition;

    private static final Tag TRANSLATIONAL_EQUIVALENCES = null;

    public Matrix getCellGramMatrixOriginal() {
        return cellGramMatrixOriginal;
    }

    public void setCellGramMatrixOriginal(Matrix cellGramMatrixOriginal) {
        this.cellGramMatrixOriginal = cellGramMatrixOriginal;
    }

    public Net(final int dim, final String name, final String group) {
        super(dim);
        this.name = name;
        this.givenGroup = group;

    }

    public Boolean getOk() {
        return isOk;
    }

    public void setOk(Boolean ok) {
        isOk = ok;
    }

    public Matrix getCellGramMatrix() {
        return cellGramMatrix;
    }

    public void setCellGramMatrix(Matrix cellGramMatrix) {
        this.cellGramMatrix = cellGramMatrix;
    }

    public String getGivenGroup() {
        return givenGroup;
    }

    public String getName() {
        return name;
    }

    public void setNodeInfo(final INode v, final Object key, final Object value) {
        assert(this.hasNode(v));
        this.nodeInfo.put(new Pair<>(v, key), value);
    }
    public String getNodeName(final INode v) {
        assert(this.hasNode(v));
        return (String) this.nodeToName.get(v);
    }

    public Map<INode, String> getNodeToNameMap() {
        return new HashMap<>(this.nodeToName);
    }

    public INode newNode() {
        final INode v = super.newNode();
        this.nodeToName.put(v, "V" + v.id());
        return v;
    }

    public INode newNode(final String name) {
        final INode v = super.newNode();
        this.nodeToName.put(v, name);
        return v;
    }

    public void delete(final INode x) {
        this.nodeToName.remove(x);
        super.delete(x);
    }

    // TODO: override to print a NetE object
    @Override
    public String toString() {
        Cover cover = conventionalCellCover();
        IteratorAdapter<IEdge> edges = cover.edges();


        String result = "Current NetE: \n";
        result += "\tName = " + getName() +"\n\n";
        result += "\tGroup = " + getGivenGroup() +"\n\n";
        result += "\tnodeToName = " + getNodeToNameMap() +"\n\n";
//	    result += "\tnodeInfo = " + nodeInfo +"\n\n";
        result += "\tnodeToPosition = " + nodeToPosition+"\n\n";
        result += "\tcellGramOriginal  = " + getCellGramMatrixOriginal() +"\n\n";
        result += "\tcellGramFinal     = " + getCellGramMatrix() +"\n\n";
        result +=  "\tUnit cell edges:\n";
        for(IEdge e: edges) {
            INode source = e.source();
            INode target = e.target();
            org.gavrog.joss.geometry.Vector s = cover.getShift(e);
            result += "\t\t" + source.id() + " " + target.id() + "  ";
            for (int j = 0; j < s.getDimension(); j++) {
                IArithmetic arithmetic = s.get(j);
                String repr = arithmetic.toString();
                String str = repr.substring(0, Math.min(2, repr.length()));
                result += str + " ";
            }
            result += "\n";

        }

        result += "\tundirectedGraph = " + super.toString() +"\n";

        return result;
    }

    public void setNodeToPosition(Map<INode,Point> nodeToPosition) {
        this.nodeToPosition = nodeToPosition;

    }

    public static Net extractNet(final String filePath)
            throws FileNotFoundException {
        // it assumes that input is a .cgd file

        final BufferedReader reader;
        reader = new BufferedReader(new FileReader(filePath));

        final NetParser parser = new NetParser(reader);

        // Block: from CRYSTAL to END
        final GenericParser.Block block = parser.parseDataBlock();
        return parser.parseNet(block);

    }
    public Cover conventionalCellCover() {
        // --- see if we can do this
        if (!isMinimal()) {
            throw new UnsupportedOperationException("graph not minimal");
        }

        try {
            return (Cover) this.cache.get(CONVENTIONAL_CELL);
        } catch (CacheMissException ex) {
            // --- construct a SpaceGroupFinder object for the symmetry group
            final SpaceGroupFinder finder = new SpaceGroupFinder(
                    getSpaceGroup());

            // --- determine a coordinate mapping into a conventional cell
            final CoordinateChange C = finder.getToStd();

            // --- express the new unit cell in terms of the old one
            final int dim = getDimension();
            final CoordinateChange Cinv = (CoordinateChange) C.inverse();
            final Vector basis[] = new Vector[dim];
            for (int i = 0; i < dim; ++i) {
                basis[i] = (Vector) Vector.unit(dim, i).times(Cinv);
            }

            // --- construct, cache and return the cover
            final Cover cover = new Cover(this, basis);
            return (Cover) cache.put(CONVENTIONAL_CELL, cover);
        }
    }
    /**
     * Checks if this graph is minimal. A periodic graph is minimal if its
     * translation group can not be extended.
     *
     * @return true if the graph if minimal.
     */
    public boolean isMinimal() {
        return !translationalEquivalenceClasses().hasNext();
    }

    /**
     * Finds all additional topological translations in this periodic graph,
     * which must be connected and locally stable and constructs the equivalence
     * classes of nodes defined by these. A topological translation in this case
     * is defined as a graph automorphism which commutes with all the given
     * translations for this periodic graph and leaves no node fixed. This
     * includes "translations" of finite orders, as they may occur in
     * ladder-like structures. A topological translation of finite order must
     * correspond to an identity transformation in the barycentric placement, so
     * it can only occur in non-stable graphs.
     *
     * An iterator is returned which contains the equivalence classes as sets if
     * additional translations are found. In the special case that none are
     * found, the iterator, however, covers the empty set rather than the set of
     * all single node sets.
     *
     * @return an iterator over the set of equivalence classes.
     */
    public Iterator<Set<INode>> translationalEquivalenceClasses() {
        return translationalEquivalences().classes();
    }

    /**
     * Computes a partition object encoding the translational equivalence
     * classes (see above).
     *
     * @return the partition into translational equivalence classes.
     */
    public Partition<INode> translationalEquivalences() {
        // --- check prerequisites
        if (!isConnected()) {
            throw new UnsupportedOperationException("graph must be connected");
        }
        if (!isLocallyStable()) {
            throw new UnsupportedOperationException("graph must be locally stable");
        }

        try {
            @SuppressWarnings("unchecked")
            final Partition<INode> result = (Partition<INode>) this.cache.get(
                    TRANSLATIONAL_EQUIVALENCES);
            return result;
        } catch (CacheMissException ex) {
        }

        final Operator I = Operator.identity(getDimension());
        final Partition<INode> P = new Partition<INode>();
        final Iterator<INode> iter = nodes();
        final INode start = iter.next();

        while (iter.hasNext()) {
            final INode v = iter.next();
            if (!P.areEquivalent(start, v)) {
                final Morphism iso;
                try {
                    iso = new Morphism(start, v, I);
                } catch (Morphism.NoSuchMorphismException ex1) {
                    continue;
                }
                for (final INode w: nodes()) {
                    P.unite(w, (INode) iso.get(w));
                }
            }
        }
        this.cache.put(TRANSLATIONAL_EQUIVALENCES, P);
        return P;
    }
    /**
     * Checks if this graph is locally stable, meaning that no two nodes with a
     * common neighbor have the same positions in a barycentric placement.
     *
     * @return true if the graph is locally stable.
     */
    public boolean isLocallyStable() {
        try {
            return (Boolean) this.cache.get(IS_LOCALLY_STABLE);
        } catch (CacheMissException ex) {
            final Map<INode, Point> positions = barycentricPlacement();
            for (final INode v: nodes()) {
                final Set<Point> positionsSeen = new HashSet<Point>();
                for (final IEdge e: v.incidences()) {
                    final Vector s = getShift(e);
                    final Point p0 = positions.get(e.target());
                    final Point p = (Point) p0.plus(s);
                    if (positionsSeen.contains(p)) {
                        this.cache.put(IS_LOCALLY_STABLE, false);
                        return false;
                    } else {
                        positionsSeen.add(p);
                    }
                }
            }
            this.cache.put(IS_LOCALLY_STABLE, true);
            return true;
        }
    }

    /**
     * Computes a barycentric placement for the nodes. Nodes are in barycentric
     * positions if each node is in the center of gravity of its neighbors. In
     * other words, each coordinate for its position is the average of the
     * corresponding coordinates for its neighbors. The barycentric positions
     * are, of course, with respect to the periodic graph. In particular, shifts
     * are taken into account. The returned map, however, contains only the
     * positions for the node representatives.
     *
     * The barycentric placement of connected graph are unique up to affine
     * transformations, i.e., general basis and origin changes. This method
     * computes coordinates expressed in terms of the basis used for edge shift
     * vectors in this graph. Moreover, the first vertex, as produced by the
     * iterator returned by nodes(), is placed at the origin.
     *
     * The graph in question must, for now, be connected as for multiple
     * components the barycentric placement is no longer unique.
     *
     * @return a map giving barycentric positions for the node representatives.
     */
    public Map<INode, Point> barycentricPlacement() {
        if (!isConnected()) {
            throw new UnsupportedOperationException("graph must be connected");
        }

        // --- see if placement has already been computed
        try {
            @SuppressWarnings("unchecked")
            Map<INode, Point> result =
                    (Map<INode, Point>) this.cache.get(BARYCENTRIC_PLACEMENT);
            return result;
        } catch (CacheMissException ex) {
        }

        // --- assign an integer index to each node representative
        final Map<INode, Integer> nodeToIndex = new HashMap<INode, Integer>();
        final List<INode> indexToNode = new ArrayList<INode>();
        for (final INode v: nodes()) {
            nodeToIndex.put(v, new Integer(indexToNode.size()));
            indexToNode.add(v);
        }

        // --- set up a system of equations
        final int n = indexToNode.size(); // the number of nodes
        final int[][] M = new int[n+1][n];
        final Matrix t = Matrix.zero(n+1, this.dimension);

        for (int i = 0; i < n; ++i) {
            final INode v = (INode) indexToNode.get(i);
            for (final IEdge e: v.incidences()) {
                final INode w = e.target();
                if (v.equals(w)) {
                    // loops cancel out with their reverses, so we must consider
                    // each loop twice (once in each direction) or not at all
                    continue;
                }
                final Vector s = getShift(e);
                final int j = nodeToIndex.get(w);
                --M[i][j];
                ++M[i][i];
                t.setRow(i, (Matrix) t.getRow(i).plus(s.getCoordinates()));
            }
        }
        M[n][0] = 1;

        // --- solve the system
        final Matrix P = Matrix.solve(new Matrix(M), t);

        // --- extract the positions found
        final Map<INode, Point> tmp = new HashMap<INode, Point>();
        for (int i = 0; i < n; ++i) {
            tmp.put(indexToNode.get(i), new Point(P.getRow(i)));
        }
        final Map<INode, Point> result = Collections.unmodifiableMap(tmp);

        // --- cache and return the result
        this.cache.put(BARYCENTRIC_PLACEMENT, result);
        return result;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////NOT RELATED TO US ////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Net(
            final PeriodicGraph graph,
            final String name,
            final String group)
    {
        super(graph.getDimension());
        final Map<INode, INode> old2new = new HashMap<INode, INode>();
        for (final INode v: graph.nodes()) {
            old2new.put(v, newNode());
        }
        for (final IEdge e: graph.edges()) {
            final INode v = old2new.get(e.source());
            final INode w = old2new.get(e.target());
            newEdge(v, w, graph.getShift(e));
        }
        this.name = name;
        this.givenGroup = group;
    }

    public Object getNodeInfo(final INode v, final Object key) {
        assert(this.hasNode(v));
        return this.nodeInfo.get(new Pair<INode, Object>(v, key));
    }

    public static Iterator<Net> iterator(final String filePath)
            throws FileNotFoundException {

        final String extension = filePath
                .substring(filePath.lastIndexOf('.') + 1);

        final BufferedReader reader;
        reader = new BufferedReader(new FileReader(filePath));

        if ("cgd".equals(extension) || "pgr".equals(extension)) {
            final NetParser parser = new NetParser(reader);

            return new Iterator<Net>() {
                public boolean hasNext() {
                    return !parser.atEnd();
                }

                public Net next() {
                    return extract(parser);
                }

                public void remove() {
                    throw new UnsupportedOperationException("not supported");
                }
            };
        } else if ("ds".equals(extension) || "tgs".equals(extension)) {
            return new FilteredIterator<Net, DSymbol>(
                    new InputIterator(reader)) {
                public Net filter(final DSymbol ds) {
                    final PeriodicGraph graph = new Tiling(ds).getSkeleton();
                    final String group = (ds.dim() == 3) ? "P1" : "p1";
                    return new Net(graph, null, group);
                }
            };
        } else if ("arc".equals(extension)) {
            return new IteratorAdapter<Net>() {
                protected Net findNext() throws NoSuchElementException {
                    final Archive.Entry entry = Archive.Entry.read(reader);
                    if (entry == null) {
                        throw new NoSuchElementException("at end");
                    }
                    final String key = entry.getKey();
                    final PeriodicGraph graph = PeriodicGraph
                            .fromInvariantString(key);
                    final String group = (graph.getDimension() == 3) ? "P1"
                            : "p1";
                    return new Net(graph, entry.getName(), group);
                }
            };
        } else {
            throw new IllegalFileNameException("Unrecognized extension \"."
                    + extension + "\"");
        }
    }

    public static class IllegalFileNameException extends RuntimeException {
        private static final long serialVersionUID = 4776009175735945241L;

        public IllegalFileNameException(final String msg) {
            super(msg);
        }
    }

    public boolean isOk() {
        return this.errors.isEmpty();
    }

    final private List<Exception> errors = new ArrayList<Exception>();

    /**
     * @param parser
     * @return
     */
    private static Net extract(final NetParser parser) {
        final GenericParser.Block data = parser.parseDataBlock();
        if (data.getType().toLowerCase().equals("tiling")) {
            final FaceList fl = new FaceList(data);
            final Tiling til = new Tiling(fl.getSymbol());
            final Map<Integer, Point> pos = fl.getPositions();
            final Tiling.Skeleton skel = til.getSkeleton();

            final Net net = new Net(
                    skel.getDimension(),
                    data.getEntriesAsString("name"),
                    "P1");

            final Map<INode, INode> old2new =
                    new HashMap<INode, INode>();
            for (final INode v: skel.nodes()) {
                old2new.put(v, net.newNode());
            }
            for (final IEdge e: skel.edges()) {
                final INode v = old2new.get(e.source());
                final INode w = old2new.get(e.target());
                net.newEdge(v, w, skel.getShift(e));
            }

            for (final int D: pos.keySet())
                net.setNodeInfo(
                        old2new.get(skel.nodeForChamber(D)),
                        NetParser.POSITION,
                        pos.get(D));
            return net;
        }
        else {
            return parser.parseNet(data);
        }
    }
    public Iterator<Exception> getErrors() {
        return this.errors.iterator();
    }
    public Iterator<String> getWarnings() {
        return warnings.iterator();
    }
    final private List<String> warnings = new ArrayList<String>();
} // end class