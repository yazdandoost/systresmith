/*
   Copyright 2012 Olaf Delgado-Friedrichs

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package org.gavrog.joss.pgraphs.io;

import org.gavrog.box.collections.NiftyList;
import org.gavrog.box.collections.Pair;
import org.gavrog.box.simple.DataFormatException;
import org.gavrog.box.simple.NamedConstant;
import org.gavrog.box.simple.TaskController;
import org.gavrog.jane.compounds.LinearAlgebra;
import org.gavrog.jane.compounds.Matrix;
import org.gavrog.jane.numbers.FloatingPoint;
import org.gavrog.jane.numbers.IArithmetic;
import org.gavrog.jane.numbers.Real;
import org.gavrog.jane.numbers.Whole;
import org.gavrog.joss.geometry.*;
import org.gavrog.joss.geometry.Vector;
import org.gavrog.joss.pgraphs.basic.IEdge;
import org.gavrog.joss.pgraphs.basic.INode;
import org.gavrog.joss.pgraphs.basic.PeriodicGraph;

import java.io.BufferedReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;


/**
 * Contains methods to parse a net specification in Systre format (file extension "cgd").
 */
public class NetParser extends GenericParser {

    public static InfoType CONNECTIVITY = new InfoType("Connectivity");
    public static InfoType COORDINATION_SEQUENCE = new InfoType("Coordination-Sequence");
    public static InfoType POSITION = new InfoType("Position");
    // The last block that was processed.
    private Block lastBlock;

    /**
     * Constructs an instance.
     *
     * @param input the input stream.
     */
    NetParser(final BufferedReader input) {
        super(input);
        this.synonyms = makeSynonyms();
        this.defaultKey = "edge";
    }

    /**
     * Sets up a keyword map to be used by {@link GenericParser#parseDataBlock()}.
     *
     * @return the mapping of keywords.
     */
    private static Map<String, String> makeSynonyms() {
        final Map<String, String> result = new HashMap<>();
        result.put("vertex", "node");
        result.put("vertices", "node");
        result.put("vertexes", "node");
        result.put("atom", "node");
        result.put("atoms", "node");
        result.put("nodes", "node");
        result.put("bond", "edge");
        result.put("bonds", "edge");
        result.put("edges", "edge");
        result.put("faces", "face");
        result.put("ring", "face");
        result.put("rings", "face");
        result.put("tiles", "tile");
        result.put("body", "tile");
        result.put("bodies", "tile");
        result.put("spacegroup", "group");
        result.put("space_group", "group");
        result.put("id", "name");
        result.put("edge_centers", "edge_center");
        result.put("edge_centre", "edge_center");
        result.put("edge_centres", "edge_center");
        result.put("edgecenter", "edge_center");
        result.put("edgecenters", "edge_center");
        result.put("edgecentre", "edge_center");
        result.put("edgecentres", "edge_center");
        result.put("coordination_sequences", "coordination_sequence");
        result.put("coordinationsequence", "coordination_sequence");
        result.put("coordinationsequences", "coordination_sequence");
        result.put("cs", "coordination_sequence");
        return Collections.unmodifiableMap(result);
    }

    /**
     * Constructs a space group with the given name.
     *
     * @param name the name of the group (as according to the International Tables).
     * @return the group constructed.
     */
    private static SpaceGroup parseSpaceGroupName(final String name) {
        final int dim;
        if (Character.isLowerCase(name.charAt(0))) {
            if (name.charAt(0) == 'o')
                dim = 1;
            else
                dim = 2;
        } else {
            dim = 3;
        }
        final Collection<Operator> ops = SpaceGroupCatalogue.operators(dim, name);
        if (ops == null) {
            return null;
        } else {
            return new SpaceGroup(dim, ops, false, false);
        }
    }

    private static Matrix convertGramMatrix(Matrix cellGram, Matrix primitiveCell) {
        if (cellGram != null) {
            cellGram = ((Matrix) primitiveCell.times(cellGram).times(primitiveCell.transposed())).symmetric();
        }
        return cellGram;
    }

    // --- extract and convert operators
    private static List<Operator> convertOperators(SpaceGroup group, Operator to, Operator from) {
        final Set<Operator> primitiveOps = group.primitiveOperators();
        List<Operator> ops = new ArrayList<>();
        for (final Operator op : primitiveOps) {
            ops.add(((Operator) from.times(op).times(to)).modZ());
        }
        return ops;
    }

    /**
     * @param groupName
     * @param dim
     * @return
     */
    private static Matrix defaultGramMatrix(final String groupName, final int dim) {
        if (dim == 1) {
            return new Matrix(new double[][]{{1.0}});
        } else if (dim == 2) {
            final char c = groupName.charAt(1);
            if (c == '3' || c == '6') {
                return new Matrix(new double[][]{
                        {1.0, -0.5},
                        {-0.5, 1.0}
                });
            } else {
                return new Matrix(new double[][]{
                        {1.0, 0.0},
                        {0.0, 1.0}
                });
            }
        } else if (dim == 3) {
            final char c;
            if (groupName.charAt(1) == '-') {
                c = groupName.charAt(2);
            } else {
                c = groupName.charAt(1);
            }
            if ((c == '3' || c == '6')
                    && !groupName.toLowerCase().endsWith(":r")) {
                return new Matrix(new double[][]{
                        {1.0, -0.5, 0.0},
                        {-0.5, 1.0, 0.0},
                        {0.0, 0.0, 1.0},
                });
            } else {
                return new Matrix(new double[][]{
                        {1.0, 0.0, 0.0},
                        {0.0, 1.0, 0.0},
                        {0.0, 0.0, 1.0},
                });
            }
        } else
            throw new RuntimeException("illegal dimension " + dim);
    }

    static private double gramMatrixError(
            int dim, SpaceGroup group, Matrix cellGram) {
        final double g[] = new double[dim * (dim + 1) / 2];
        int k = 0;
        for (int i = 0; i < dim; ++i) {
            for (int j = i; j < dim; ++j) {
                g[k] = ((Real) cellGram.get(i, j)).doubleValue();
                ++k;
            }
        }
        final Matrix S = group.configurationSpaceForGramMatrix();
        final Matrix A = new Matrix(new double[][]{g});
        final Matrix M = LinearAlgebra.solutionInRows(S, A, false);
        if (M == null)
            return 1;
        final Matrix D = (Matrix) M.times(S).minus(A);
        return ((Real) D.norm()).doubleValue();
    }

    /**
     * Constructs a gram matrix for the edge vectors of a unit cell which is specified by
     * its cell parameters as according to crystallographic conventions.
     *
     * @param dim            the dimension of the cell.
     * @param cellParameters the list of cell parameters.
     * @return the gram matrix for the vectors.
     */
    private static Matrix gramMatrix(int dim,
                                     final List<Object> cellParameters) {
        if (dim == 2) {
            final Real a = (Real) cellParameters.get(0);
            final Real b = (Real) cellParameters.get(1);
            final Real angle = (Real) cellParameters.get(2);
            final Real x = (Real) cosine(angle).times(a).times(b);

            return new Matrix(new IArithmetic[][]{{a.raisedTo(2), x},
                    {x, b.raisedTo(2)}});
        } else if (dim == 3) {
            final Real a = (Real) cellParameters.get(0);
            final Real b = (Real) cellParameters.get(1);
            final Real c = (Real) cellParameters.get(2);
            final Real alpha = (Real) cellParameters.get(3);
            final Real beta = (Real) cellParameters.get(4);
            final Real gamma = (Real) cellParameters.get(5);

            final Real alphaG = (Real) cosine(alpha).times(b).times(c);
            final Real betaG = (Real) cosine(beta).times(a).times(c);
            final Real gammaG = (Real) cosine(gamma).times(a).times(b);

            return new Matrix(
                    new IArithmetic[][]{{a.raisedTo(2), gammaG, betaG},
                            {gammaG, b.raisedTo(2), alphaG},
                            {betaG, alphaG, c.raisedTo(2)},});
        } else {
            throw new DataFormatException("supporting only dimensions 2 and 3");
        }
    }

    /**
     * Computes the cosine of an angle given in degrees, using the {@link Real} type for
     * the argument and return value.
     *
     * @param arg the angle in degrees.
     * @return the value of the cosine.
     */
    private static Real cosine(final Real arg) {
        final double f = Math.PI / 180.0;
        return new FloatingPoint(Math.cos(arg.doubleValue() * f));
    }

    /**
     * Computes the stabilizer of a site modulo lattice translations.The infinity norm
     * (largest absolute value of a matrix entry) is used to determine the distances
     * between points.
     * <p>
     * Currently only tested for point sites.
     *
     * @param site      the site.
     * @param ops       operators forming the symmetry group.
     * @param precision points this close are considered equal.
     * @return the set of operators forming the stabilizer
     */
    private static Set<Operator> pointStabilizer(
            final Point site, final List<Operator> ops, final double precision) {
        final Set<Operator> stabilizer = new HashSet<>();

        for (final Operator op : ops) {
            final double dist = distModZ(site, (Point) site.times(op));
            if (dist <= precision) { // using "<=" allows for precision 0
                stabilizer.add(op.modZ());
            }
        }

        // --- check if stabilizer forms a group
        if (!formGroup(stabilizer)) {
            throw new RuntimeException("precision problem in stabilizer computation");
        }

        return stabilizer;
    }

    /**
     * Measures the distance between two sites in terms of the infinity norm of
     * the representing matrices. The distance is computed modulo Z^d, where Z
     * is the dimension of the sites, thus, sites are interpreted as residing in
     * the d-dimensional torus.
     * <p>
     * Currently only implemented for point sites.
     *
     * @param site1 first point site.
     * @param site2 second point site.
     * @return the distance.
     */
    private static double distModZ(final Point site1, final Point site2) {
        final int dim = site1.getDimension();
        final Vector diff = (Vector) site1.minus(site2);
        double maxD = 0.0;
        for (int j = 0; j < dim; ++j) {
            final double d = ((Real) diff.get(j).mod(Whole.ONE)).doubleValue();
            maxD = Math.max(maxD, Math.min(d, 1.0 - d));
        }
        return maxD;
    }

    /**
     * Determines if the given operators form a group modulo Z^d.
     *
     * @param operators a collection of operators.
     * @return true if the operators form a group.
     */
    final static boolean formGroup(final Collection<Operator> operators) {
        for (final Operator A : operators) {
            for (final Operator B : operators) {
                final Operator AB_ = ((Operator) A.times(B.inverse())).modZ();
                if (!operators.contains(AB_)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Parses a pre-parsed data block and returns the net specified by it.
     *
     * @param block the data block to parse.
     * @return the periodic net constructed from the input.
     */
    public Net parseNet(final Block block) {
        final Entry entries[] = block.getEntries();
        // entry: is a unit in the cgd file, such as NAME, GROUP, CELL etc.
        if (entries == null) {
            return null;
        }
        this.lastBlock = block; // TODO: check if used
        return parseCrystal(entries);
    }

    /**
     * Retrieves the name of the net last read, if any.
     *
     * @return everything present under the "name" or "id" key.
     */
    private String getName() {
        return lastBlock.getEntriesAsString("name");
    }

    /**
     * Retrieves the spacegroup given for the net last read, if any.
     *
     * @return everything present under the "name" of "id" key.
     */
    private String getSpaceGroup() {
        final String group = lastBlock.getEntriesAsString("group");
        if (group == null) {
            return "P1";
        } else {
            return group;
        }
    }

    /**
     * Generates a warning message indicating .
     *
     * @param entry
     * @return
     */
    private String keywordWarning(final Entry entry) {
        return "Unknown keyword '" + entry.originalKey + "' at line " + entry.lineNumber;
    }

    /**
     * Parses a crystal descriptor and constructs the corresponding atom-bond
     * network.
     * <p>
     * Example:
     * <p>
     * <pre>
     * CRYSTAL
     *   GROUP Fd-3m
     *   CELL         2.3094 2.3094 2.3094  90.0 90.0 90.0
     *   ATOM  1  4   5/8 5/8 5/8
     * END
     * </pre>
     *
     * @param block the pre-parsed input.
     * @return the periodic graph constructed from the input.
     */
    private Net parseCrystal(final Entry[] block) {

        int dim;
        SpaceGroup group;
        List<Operator> ops;
        Matrix cellGram;
        Matrix cellGramOriginal;

        double precision = 0.001;
        double minEdgeLength = 0.1;

        List<NodeDescriptor> nodeDescriptors;
        List<NodeDescriptor> nodeDescriptorsOriginal;
        Map<Object, NodeDescriptor> nameToDesc;
        final List<List<Object>> coordinationSeqs = new LinkedList<>();


        // --- collect data from the input
        NetInfoHelper netInfoHelper = new NetInfoHelper(block).parseCgdFile();
        group = netInfoHelper.getGroup();
        dim = netInfoHelper.getDim();
        cellGram = netInfoHelper.getCellGram();
        cellGramOriginal = new Matrix(cellGram);
        nodeDescriptors = netInfoHelper.getNodeDescriptors();
        nodeDescriptorsOriginal = netInfoHelper.getNodeDescriptors();
        nameToDesc = netInfoHelper.getNameToDesc();

        // --- assign coordination sequences to node names
        final Map<Object, List<Object>> name2cs = createNameToCoordinationSeq(nodeDescriptors, coordinationSeqs);

        // --- get info for converting to a primitive setting
        final Matrix primitiveCell = group.primitiveCell();
        final Operator to = group.transformationToPrimitive();
        final Operator from = (Operator) to.inverse();


        ops = convertOperators(group, to, from);

        nodeDescriptors = convertNodeDescriptors(nodeDescriptorsOriginal, nameToDesc, to);

        // NOTE: not needed right now since we do not read edges from the cgd file
//        convertEdgeDescriptors(edgeDescriptors, to);

        cellGram = convertGramMatrix(cellGram, primitiveCell);

        // --- apply group operators to generate all nodes
        final List<Pair<NodeDescriptor, Operator>> allNodes = applyOps(ops, nodeDescriptors, precision);

        // --- Create a net with the computed nodes
        final Net G = new Net(dim, getName(), getSpaceGroup());
        G.setCellGramMatrix(cellGram);
        G.setCellGramMatrixOriginal(cellGramOriginal);
        final Map<INode, Pair<NodeDescriptor, Operator>> nodeToDescriptorAddress = new HashMap<>();

        for (final Pair<NodeDescriptor, Operator> adr : allNodes) {
            final NodeDescriptor desc = adr.getFirst();
            final INode v = G.newNode("" + desc.name);
            G.setNodeInfo(v, CONNECTIVITY, desc.connectivity);
            G.setNodeInfo(v, COORDINATION_SEQUENCE, name2cs.get(desc.name));
            nodeToDescriptorAddress.put(v, adr);
        }

        // --- Map nodes to positions
        final Map<INode, Point> nodeToPosition = createNodesToPositions(nodeToDescriptorAddress);

        // NOTE: not needed right now since we do not read edges from the cgd file
        // --- Handle explicit edges
//        addExplicitEdges(G, edgeDescriptors, ops, nameToDesc, nodeToPosition, from, precision);

        // --- Compute implicit edges

        computeImplicitEdges(G, cellGram, nodeToDescriptorAddress, nodeToPosition, minEdgeLength);

        // --- remove nodes that are really meant to be edge centers
        removeEdgeCenters(G, nodeToDescriptorAddress);

        // Store the original positions for all nodes.
        for (final INode v : G.nodes()) {
            G.setNodeInfo(v, POSITION, nodeToPosition.get(v));
        }
        G.setNodeToPosition(nodeToPosition);
        return G;
    }

    private Map<INode, Point> createNodesToPositions(Map<INode, Pair<NodeDescriptor, Operator>> nodeToDescriptorAddress) {
        final Map<INode, Point> nodeToPosition = new HashMap<>();

        for (final INode v : nodeToDescriptorAddress.keySet()) {
            final Pair<NodeDescriptor, Operator> adr = nodeToDescriptorAddress.get(v);
            nodeToPosition.put(v, (Point) adr.getFirst().site.times(adr.getSecond()));
        }
        return nodeToPosition;
    }

    private List<NodeDescriptor> convertNodeDescriptors(List<NodeDescriptor> nodeDescriptors, Map<Object, NodeDescriptor> nameToDesc, Operator to) {
        final List<NodeDescriptor> nodeDescsTmp = new LinkedList<>();
        for (final NodeDescriptor desc : nodeDescriptors) {
            final NodeDescriptor newDesc = new NodeDescriptor(desc.name, desc.connectivity, desc.site.times(to), desc.isEdgeCenter);
            nodeDescsTmp.add(newDesc);
            nameToDesc.put(desc.name, newDesc);
        }

        return nodeDescsTmp;
    }

    private Map<Object, List<Object>> createNameToCoordinationSeq(List<NodeDescriptor> nodeDescriptors, List<List<Object>> coordinationSeqs) {
        final Map<Object, List<Object>> name2cs = new HashMap<>();
        for (int i = 0; i < coordinationSeqs.size(); ++i) {
            name2cs.put(nodeDescriptors.get(i).name, coordinationSeqs.get(i));
        }
        return name2cs;
    }

    /**
     * @param G
     * @param cellGram
     * @param nodeToDescriptorAddress
     * @param nodeToPosition
     * @param minEdgeLength
     */
    private void computeImplicitEdges(
            final Net G,
            final Matrix cellGram,
            final Map<INode, Pair<NodeDescriptor, Operator>> nodeToDescriptorAddress,
            final Map<INode, Point> nodeToPosition,
            final double minEdgeLength) {

        // --- construct a Dirichlet domain for the translation group
        final Vector basis[] = Vector.rowVectors(Matrix.one(G.getDimension()));
        final Vector dirichletVectors[] = Lattices.dirichletVectors(basis, cellGram);

        // --- shift generated nodes into the Dirichlet domain
        for (final INode v : nodeToPosition.keySet()) {
            final Point p = nodeToPosition.get(v);
            // --- shift into Dirichlet domain
            final Vector shift = Lattices.dirichletShifts(p, dirichletVectors, cellGram, 1)[0];
            // NOTE: this is where the position of nodes change
            nodeToPosition.put(v, (Point) p.plus(shift));
            G.shiftNode(v, shift);
        }

        // --- compute nodes in two times extended Dirichlet domain
        final Vector zero = Vector.zero(G.getDimension());

        final List<Pair<INode, Vector>> extended = new ArrayList<>();
        final Map<Pair<INode, Vector>, Point> addressToPosition = new HashMap<>();
        for (final INode v : G.nodes()) {
            TaskController.getInstance().bailOutIfCancelled();
            final Point pv = nodeToPosition.get(v);

            extended.add(new Pair<>(v, zero));
            addressToPosition.put(new Pair<>(v, zero), pv);
            for (final Vector vec : dirichletVectors) {
                final Point p = (Point) pv.plus(vec);
                final Vector shifts[] = Lattices.dirichletShifts(p, dirichletVectors, cellGram, 2);

                for (final Vector shift : shifts) {
                    final Pair<INode, Vector> adr = new Pair<>(v, (Vector) vec.plus(shift));
                    extended.add(adr);
                    addressToPosition.put(adr, (Point) p.plus(shift));
                }
            }
        }
        // NOTE: Not yet used
        // --- compute potential edges
        final List<Pair<IArithmetic, Pair<INode, Integer>>> edges = new ArrayList<>();
        for (final INode v : G.nodes()) {
            TaskController.getInstance().bailOutIfCancelled();
            final NodeDescriptor descV = nodeToDescriptorAddress.get(v).getFirst();
            final Pair<INode, Vector> adr0 = new Pair<>(v, zero);
            final Point pv = nodeToPosition.get(v);
            final List<Pair<IArithmetic, Integer>> distances = new ArrayList<>();
            for (int i = 0; i < extended.size(); ++i) {
                TaskController.getInstance().bailOutIfCancelled();
                final Pair<INode, Vector> adr = extended.get(i);
                if (adr.equals(adr0)) {
                    continue;
                }
                final NodeDescriptor descW = nodeToDescriptorAddress.get(adr.getFirst()).getFirst();
                if (descV.isEdgeCenter && descW.isEdgeCenter) {
                    continue;
                }

                final Point pos = addressToPosition.get(adr);
                final Vector diff0 = (Vector) pos.minus(pv);
                final Matrix diff = diff0.getCoordinates();
                final IArithmetic dist = LinearAlgebra.dotRows(diff, diff, cellGram);
                distances.add(new Pair<>(dist, i));
            }
            Collections.sort(distances, Pair.defaultComparator());

            for (int i = 0; i < descV.connectivity; ++i) {
                final Pair<IArithmetic, Integer> entry = distances.get(i);
                final IArithmetic dist = entry.getFirst();
                final Integer k = entry.getSecond();
                edges.add(new Pair<>(dist, new Pair<>(v, k)));
            }
        }

        // --- sort potential edges by length
        Collections.sort(edges, Pair.firstItemComparator());

        // --- add eges shortest to longest until all nodes are saturated
        for (final Pair<IArithmetic, Pair<INode, Integer>> edge : edges) {
            final double dist = ((Real) edge.getFirst()).doubleValue();
            final Pair<INode, Integer> ends = edge.getSecond();
            final INode v = ends.getFirst();
            final Pair<INode, Vector> adr = extended.get(ends.getSecond());
            final INode w = adr.getFirst();
            final Vector s = adr.getSecond();

            final NodeDescriptor descV = nodeToDescriptorAddress.get(v).getFirst();
            final NodeDescriptor descW = nodeToDescriptorAddress.get(w).getFirst();

            if (v.degree() >= descV.connectivity && w.degree() >= descW.connectivity) {
                continue;
            }
            if (dist < minEdgeLength) {
                final String msg = "Found points closer than minimal edge length of ";
                throw new DataFormatException(msg + minEdgeLength);
            }
            if (G.getEdge(v, w, s) == null) {
                G.newEdge(v, w, s);
            }
            if (v.degree() > descV.connectivity) {
                final String msg = "Found " + v.degree()
                        + " neighbors for node " + descV.name + " (should be "
                        + descV.connectivity + ")";
                throw new DataFormatException(msg);
            }
            if (w.degree() > descW.connectivity) {
                final String msg = "Found " + w.degree()
                        + " neighbors for node " + descW.name + " (should be "
                        + descW.connectivity + ")";
                throw new DataFormatException(msg);
            }
        }
    }

    /**
     * @param G
     * @param nodeToAdr
     */
    private void removeEdgeCenters(
            final Net G,
            final Map<INode, Pair<NodeDescriptor, Operator>> nodeToAdr) {
        final Set<INode> bogus = new HashSet<>();
        for (final INode v : G.nodes()) {
            if (nodeToAdr.get(v).getFirst().isEdgeCenter) {
                bogus.add(v);
            }
        }
        for (final INode v : bogus) {
            final List<IEdge> inc = G.allIncidences(v);
            if (inc.size() != 2) {
                throw new DataFormatException("Edge center has connectivity != 2");
            }
            final IEdge e1 = inc.get(0);
            final INode w1 = e1.opposite(v);
            final IEdge e2 = inc.get(1);
            final INode w2 = e2.opposite(v);
            final Vector shift = (Vector) G.getShift(e2).minus(G.getShift(e1));
            if (G.getEdge(w1, w2, shift) != null) {
                throw new DataFormatException("duplicate edge");
            } else if (w1.equals(w2) && shift.equals(shift.zero())) {
                throw new DataFormatException("trivial loop");
            }
            G.newEdge(w1, w2, shift);
            G.delete(e1);
            G.delete(e2);
            G.delete(v);
        }
    }

    /**
     * @param ops
     * @param nodeDescriptors
     * @param precision
     * @return
     */
    private List<Pair<NodeDescriptor, Operator>> applyOps(
            final List<Operator> ops,
            final List<NodeDescriptor> nodeDescriptors,
            final double precision) {
        final List<Pair<NodeDescriptor, Operator>> allNodes = new LinkedList<>();

        for (final NodeDescriptor desc : nodeDescriptors) {
            final Point site = (Point) desc.site;
            final Set<Operator> stabilizer = pointStabilizer(site, ops, precision);
            // --- loop through the cosets of the stabilizer
            final Set<Operator> opsSeen = new HashSet<>();
            for (final Operator oper : ops) {
                // --- get the next coset representative
                final Operator op = oper.modZ();
                if (!opsSeen.contains(op)) {
                    allNodes.add(new Pair<>(desc, op));
                    // --- mark operators that should not be used anymore
                    for (final Operator op1 : stabilizer) {
                        final Operator a = ((Operator) op1.times(op)).modZ();
                        opsSeen.add(a);
                    }
                }
            }
        }
        return allNodes;
    }

    // --- define some key constants for data associated to nodes
    private static class InfoType extends NamedConstant {
        private InfoType(final String name) {
            super(name);
        }
    }

    /**
     * Helper class - encapsulates the preliminary specification of a node.
     */
    private static class NodeDescriptor {
        public final Object name;     // the node's name
        final int connectivity; // the node's connectivity
        final IArithmetic site;     // the position or site of the node
        boolean isEdgeCenter; // is this really an edge center?

        NodeDescriptor(final Object name, final int connectivity, final IArithmetic site, final boolean isEdgeCenter) {
            this.name = name;
            this.connectivity = connectivity;
            this.site = site;
            this.isEdgeCenter = isEdgeCenter;
        }

        public String toString() {
            if (isEdgeCenter) {
                return "EdgeCenter(" + name + ", " + connectivity + ", " + site + ")";
            } else {
                return "Node(" + name + ", " + connectivity + ", " + site + ")";
            }
        }
    }

    private class NetInfoHelper {
        private Entry[] block;
        private String groupName;
        private int dim;
        private SpaceGroup group;
        private List<Operator> ops;
        private Matrix cellGram;
        private List<NodeDescriptor> nodeDescriptors;
        private Map<Object, NodeDescriptor> nameToDesc;
        private List<String> warnings;

        public NetInfoHelper(Entry[] block) {
            this.block = block;
            this.dim = 3;
            this.ops = new ArrayList<>();
            this.nodeDescriptors = new LinkedList<>();
            this.nameToDesc = new HashMap<>();
            this.warnings = new ArrayList<>();
        }

        public List<Operator> getOps() {
            return ops;
        }

        public void setOps(List<Operator> ops) {
            this.ops = ops;
        }

        public List<NodeDescriptor> getNodeDescriptors() {
            return nodeDescriptors;
        }

        public Map<Object, NodeDescriptor> getNameToDesc() {
            return nameToDesc;
        }

        public int getDim() {
            return dim;
        }

        public SpaceGroup getGroup() {
            return group;
        }

        public void setGroup(SpaceGroup group) {
            this.group = group;
        }

        public Matrix getCellGram() {
            return cellGram;
        }

        public NetInfoHelper parseCgdFile() {
            // this for loop just parses what is in cgd file and the cellGram is computed, the point is still fractional (as in original file)- the symmetry is not applied yet
            // NOTE: we do not parse edges. If later needed, get code from netParser.java
            final Set<String> seen = new HashSet<>();
            for (int i = 0; i < block.length; ++i) {
                final List<Object> row = block[i].values;
                final String key = block[i].key;
                final int lineNr = block[i].lineNumber;
                if (key.equals("group")) {
                    if (seen.contains(key)) {
                        final String msg = "Group specified twice at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    if (row.size() < 1) {
                        final String msg = "Missing argument at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    groupName = (String) row.get(0);
                    group = parseSpaceGroupName(groupName);
                    if (group == null) {
                        final String msg = "Space group \"" + groupName
                                + "\" not recognized at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    dim = group.getDimension();
                    groupName = SpaceGroupCatalogue.listedName(dim, groupName);
                    ops.addAll(group.getOperators());
                } else if (key.equals("cell")) {
                    if (seen.contains(key)) {
                        final String msg = "Cell specified twice at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    final int m = dim + dim * (dim - 1) / 2;
                    if (row.size() != m) {
                        final String msg = "Expected " + m + " arguments at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    for (int j = 0; j < m; ++j) {
                        if (!(row.get(i) instanceof Real)) {
                            final String msg = "Arguments must be real numbers at line ";
                            throw new DataFormatException(msg + lineNr);
                        }
                    }
                    cellGram = gramMatrix(dim, row);
                    // TODO: comment the edge_center for now to make sure it is useless
                } else if (key.equals("node")) {
                    if (row.size() != dim + 2) { // Err check
                        final String msg = "Expected " + (dim + 2) + " arguments at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    final Object name = row.get(0);
                    if (nameToDesc.containsKey(name)) {
                        final String msg = "Node specified twice at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    final Object conn = row.get(1);
                    if (!(conn instanceof Whole && ((Whole) conn).isNonNegative())) { // err check: valency must be positive
                        final String msg = "Connectivity must be a non-negative integer at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                    final IArithmetic pos[] = new IArithmetic[dim];
                    for (int j = 0; j < dim; ++j) {
                        pos[j] = (IArithmetic) row.get(j + 2);
                    }
                    final int c = Math.max(0, ((Whole) conn).intValue());
                    final boolean isCenter = false;
                    final NodeDescriptor node = new NodeDescriptor(name, c, new Point(pos), isCenter);
                    nodeDescriptors.add(node);
                    nameToDesc.put(name, node);
                } else if (!key.equals("name")) {
                    warnings.add(keywordWarning(block[i]));
                }
                seen.add(key);
            }

            // --- validate and use reasonable default for missing data
            if (group == null) {
                warnings.add("No space group given - assuming P1");
                groupName = "P1";
                group = parseSpaceGroupName(groupName);
                assert group != null;
                dim = group.getDimension();
                ops.addAll(group.getOperators());
            }
            if (cellGram == null) {
                cellGram = defaultGramMatrix(groupName, dim);
            }
            // --- warn about illegal cell parameters
            if (gramMatrixError(dim, group, cellGram) > 0.01)
                warnings.add("Unit cell parameters illegal for this group");

            return this;
        }
    }


    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////NOT RELATED TO US ////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public static class Face implements Comparable<Face> {
        final private int size;
        final private int vertices[];
        final private Vector shifts[];

        public Face(final int points[], final Vector shifts[]) {
            if (points.length != shifts.length) {
                throw new RuntimeException("lengths do not match");
            }
            this.vertices = (int[]) points.clone();
            this.shifts = (Vector[]) shifts.clone();
            this.size = shifts.length;
        }

        public int vertex(final int i) {
            return this.vertices[i];
        }
        public Vector shift(final int i) {
            return this.shifts[i];
        }
        public int size() {
            return this.size;
        }

        public int hashCode() {
            int code = 0;
            for (int i = 0; i < size(); ++i) {
                code = (code * 37 + vertex(i)) * 127 + shift(i).hashCode();
            }
            return code;
        }

        public int compareTo(final Face f) {
            int d = 0;
            for (int i = 0; i < size(); ++i) {
                d = vertex(i) - f.vertex(i);
                if (d != 0) {
                    return d;
                }
                d = shift(i).compareTo(f.shift(i));
                if (d != 0) {
                    return d;
                }
            }
            return 0;
        }

        public boolean equals(final Object other) {
            if (other instanceof Face)
                return compareTo((Face) other) == 0;
            else
                return false;
        }

        public String toString() {
            final  StringBuffer buf = new StringBuffer(100);
            for (int i = 0; i < size(); ++i) {
                if (i > 0) {
                    buf.append('-');
                }
                buf.append('(');
                buf.append(vertex(i));
                buf.append(',');
                buf.append(shift(i).toString().replaceFirst("Vector", ""));
                buf.append(')');
            }
            return buf.toString();
        }
    }

    /**
     * Parses a list of rings.
     *
     * @param block the pre-parsed input.
     * @return the ring list in symbolic form.
     */
    private static FaceListDescriptor parseFaceList(final Entry[] block) {
        final Set<String> seen = new HashSet<String>();

        String groupName = null;
        int dim = 3;
        SpaceGroup group = null;
        List<Operator> ops = new ArrayList<Operator>();
        Matrix cellGram = null;

        double precision = 0.001;
        boolean useTiles = false;
        final List<List<Point[]>> faceLists = new ArrayList<List<Point[]>>();
        List<Point[]> faces = new ArrayList<Point[]>();
        IArithmetic faceData[] = null;
        int faceDataIndex = 0;

        final List<String> warnings = new ArrayList<String>();

        // --- collect data from the input
        for (int i = 0; i < block.length; ++i) {
            final List<Object> row = block[i].values;
            final String key = block[i].key;
            final int lineNr = block[i].lineNumber;
            if (key.equals("group")) {
                if (seen.contains(key)) {
                    final String msg = "Group specified twice at line ";
                    throw new DataFormatException(msg + lineNr);
                }
                if (row.size() < 1) {
                    final String msg = "Missing argument at line ";
                    throw new DataFormatException(msg + lineNr);
                }
                groupName = (String) row.get(0);
                group = parseSpaceGroupName(groupName);
                if (group == null) {
                    final String msg = "Space group \"" + groupName
                            + "\" not recognized at line ";
                    throw new DataFormatException(msg + lineNr);
                }
                dim = group.getDimension();
                groupName = SpaceGroupCatalogue.listedName(dim, groupName);
                ops.addAll(group.getOperators());
            } else if (key.equals("cell")) {
                if (seen.contains(key)) {
                    final String msg = "Cell specified twice at line ";
                    throw new DataFormatException(msg + lineNr);
                }
                final int m = dim + dim * (dim-1) / 2;
                if (row.size() != m) {
                    final String msg = "Expected " + m + " arguments at line ";
                    throw new DataFormatException(msg + lineNr);
                }
                for (int j = 0; j < m; ++j) {
                    if (!(row.get(i) instanceof Real)) {
                        final String msg = "Arguments must be real numbers at line ";
                        throw new DataFormatException(msg + lineNr);
                    }
                }
                cellGram = gramMatrix(dim, row);
            } else if (key.equals("face")) {
                for (int j = 0; j < row.size(); ++j) {
                    final Object item = row.get(j);
                    if (faceData == null) {
                        if (item instanceof Whole) {
                            final int n = ((Whole) item).intValue();
                            faceData = new IArithmetic[n * dim];
                            faceDataIndex = 0;
                        } else {
                            String msg = "face size expected at line ";
                            throw new DataFormatException(msg + lineNr);
                        }
                    } else {
                        if (item instanceof IArithmetic) {
                            faceData[faceDataIndex++] = (IArithmetic) item;
                            if (faceDataIndex == faceData.length) {
                                final int n = faceData.length / dim;
                                final Point f[] = new Point[n];
                                int p = 0;
                                for (int nu = 0; nu < n; ++nu) {
                                    IArithmetic pos[] = new IArithmetic[dim];
                                    for (int k = 0; k < dim; ++k) {
                                        pos[k] = faceData[p++];
                                    }
                                    f[nu] = new Point(pos);
                                }
                                faces.add(f);
                                faceData = null;
                            }
                        } else {
                            String msg = "coordinate expected at line ";
                            throw new DataFormatException(msg + lineNr);
                        }
                    }
                }
            } else if (key.equals("tile")) {
                useTiles = true;
                if (faces.size() > 0) {
                    faceLists.add(faces);
                }
                faces = new ArrayList<Point[]>();
            } else {
                // store additional entrys here
            }
            seen.add(key);
        }
        if (faces.size() > 0) {
            faceLists.add(faces);
        }

        // --- use reasonable default for missing data
        if (group == null) {
            warnings.add("No space group given - assuming P1");
            groupName = "P1";
            group = parseSpaceGroupName(groupName);
            dim = group.getDimension();
            ops.addAll(group.getOperators());
        }

        // --- warn about illegal cell parameters
        if (cellGram != null && gramMatrixError(dim, group, cellGram) > 0.01)
            warnings.add("Unit cell parameters illegal for this group");

        // --- get info for converting to a primitive setting
        final Matrix primitiveCell = group.primitiveCell();
        final Operator to = group.transformationToPrimitive();
        final Operator from = (Operator) to.inverse();

        // --- extract and convert operators
        final Set<Operator> primitiveOps = group.primitiveOperators();
        ops.clear();
        for (final Operator op: primitiveOps) {
            ops.add(((Operator) from.times(op).times(to)).modZ());
        }

        // --- convert face lists
        for (final List<Point[]> list: faceLists) {
            for (int i = 0; i < list.size(); ++i) {
                final Point faceOld[] = list.get(i);
                final Point faceNew[] = new Point[faceOld.length];
                for (int k = 0; k < faceOld.length; ++k) {
                    faceNew[k] = (Point) faceOld[k].times(to);
                }
                list.set(i, faceNew);
            }
        }

        // --- convert gram matrix
        if (cellGram != null) {
            cellGram = ((Matrix) primitiveCell.times(cellGram).times(
                    primitiveCell.transposed())).symmetric();
        }

        // --- apply group operators to generate all corner points
        final Map<Integer, Point> indexToPos = new HashMap<Integer, Point>();

        for (final List<Point[]> list: faceLists) {
            for (final Point face[]: list) {
                for (int i = 0; i < face.length; ++i) {
                    final Point site = face[i];
                    if (lookup(site, indexToPos, precision) != null) {
                        continue;
                    }

                    final Set<Operator> stabilizer =
                            pointStabilizer(site, ops, precision);
                    // --- loop through the cosets of the stabilizer
                    final Set<Operator> opsSeen = new HashSet<Operator>();
                    for (final Operator oper: ops) {
                        // --- get the next coset representative
                        final Operator op = oper.modZ();
                        if (!opsSeen.contains(op)) {
                            // --- compute mapped node position
                            final Point p = (Point) site.times(op);
                            indexToPos.put(indexToPos.size(), p);

                            // --- mark operators that should not be used anymore
                            for (final Operator op2: stabilizer) {
                                final Operator a = (Operator) op2.times(op);
                                final Operator aModZ = a.modZ();
                                opsSeen.add(aModZ);
                            }
                        }
                    }
                }
            }
        }


        // Record symmetry actions on corner points
        final Map<Pair<Operator, Integer>, Pair<Integer, Vector>> actions =
                new HashMap<Pair<Operator, Integer>, Pair<Integer, Vector>>();

        for (final Operator oper: ops) {
            for (final int i: indexToPos.keySet()) {
                final Point p = (Point) indexToPos.get(i).times(oper);
                actions.put(new Pair<Operator, Integer>(oper, i),
                        lookup(p, indexToPos, precision));
            }
        }

        // Apply the symmetries to faces and tiles
        final Set<Object> notNew = new HashSet<Object>();
        final List<Object> result = new ArrayList<Object>();
        for (final List<Point[]> list: faceLists) {
            for (final Operator oper: ops) {
                final Operator op = oper.modZ();
                final List<Pair<Face, Vector>> mappedFaces =
                        new ArrayList<Pair<Face, Vector>>();

                for (final Point f[]: list) {
                    final int n = f.length;
                    final int points[] = new int[n];
                    final Vector shifts[] = new Vector[n];
                    for (int i = 0; i < n; ++i) {
                        final Pair<Integer, Vector> p =
                                lookup((Point) f[i].times(op),
                                        indexToPos, precision);
                        points[i] = p.getFirst();
                        shifts[i] = p.getSecond();
                    }
                    final Face fMapped = new Face(points, shifts);
                    final Pair<Face, Vector> normalized =
                            normalizedFace(fMapped);
                    if (useTiles) {
                        mappedFaces.add(normalized);
                    } else {
                        final Face fNormal = normalized.getFirst();
                        if (notNew.contains(fNormal)) {
                        } else {
                            result.add(fNormal);
                            notNew.add(fNormal);
                        }
                    }
                }
                if (useTiles) {
                    final List<Pair<Face, Vector>> tNormal =
                            normalizedTile(mappedFaces);
                    if (notNew.contains(tNormal)) {
                    } else {
                        result.add(tNormal);
                        notNew.add(tNormal);
                    }
                }
            }
        }

        return new FaceListDescriptor(
                result, indexToPos, ops, actions, cellGram);
    }
    public static FaceListDescriptor parseFaceList(final Block block) {
        return parseFaceList(block.getEntries());
    }
    /**
     * @param face
     * @return the normalized form of the given face.
     */
    public static Pair<Face, Vector> normalizedFace(final Face face) {
        final int n = face.size();
        Face trial;
        Face best = null;
        Vector bestShift = null;
        for (int i = 0; i < n; ++i) {
            final Vector s = face.shift(i);
            int points[] = new int[n];
            Vector shifts[] = new Vector[n];
            for (int k = 0; k < n; ++k) {
                final int index = (i + k) % n;
                final int v = face.vertex(index);
                final Vector t = face.shift(index);
                points[k] = v;
                shifts[k] = (Vector) t.minus(s);
            }
            trial = new Face(points, shifts);
            for (int r = 0; r <= 1; ++r) {
                if (best == null || best.compareTo(trial) > 0) {
                    best = trial;
                    bestShift = s;
                }
                for (int k = 1; k  < (n + 1) / 2; ++k) {
                    final int t = points[k];
                    points[k] = points[n - k];
                    points[n - k] = t;
                    final Vector tmp = shifts[k];
                    shifts[k] = shifts[n - k];
                    shifts[n - k] = tmp;
                }
                trial = new Face(points, shifts);
            }
        }

        return new Pair<Face, Vector>(best, bestShift);
    }
    private static List<Pair<Face, Vector>> normalizedTile(
            final List<Pair<Face, Vector>> tile) {

        final Comparator<Pair<Face, Vector>> pairComparator =
                Pair.<Face, Vector>defaultComparator();

        final Comparator<List<Pair<Face, Vector>>> listComparator =
                NiftyList.<Pair<Face, Vector>>
                        lexicographicComparator(pairComparator);

        List<Pair<Face, Vector>> best = null;
        for (int i = 0; i < tile.size(); ++i) {
            final Vector shift = tile.get(i).getSecond();
            final List<Pair<Face, Vector>> current =
                    new ArrayList<Pair<Face, Vector>>();
            for (final Pair<Face, Vector> pair: tile) {
                final Face face = pair.getFirst();
                final Vector t = pair.getSecond();
                current.add(
                        new Pair<Face, Vector>(face, (Vector) t.minus(shift)));
            }
            Collections.sort(current, pairComparator);

            if (best == null || listComparator.compare(best, current) < 0) {
                best = current;
            }
        }

        return best;
    }
    /**
     * Finds the node and shift associated to a point position.
     * @param pos the position to look up.
     * @param keyToPos maps nodes to positions.
     * @param precision how close must points be to considered equal.
     *
     * @return the (node, shift) pair found or else null.
     */
    private static <K> Pair<K, Vector> lookup(
            final Point pos,
            final Map<K, Point> keyToPos,
            final double precision)
    {
        final int d = pos.getDimension();
        for (final K v: keyToPos.keySet()) {
            final Point p = keyToPos.get(v);
            if (distModZ(pos, p) <= precision) {
                final Vector diff = (Vector) pos.minus(p);
                final int s[] = new int[d];
                for (int i = 0; i < d; ++i) {
                    final double x = ((Real) diff.get(i)).doubleValue();
                    s[i] = (int) Math.round(x);
                }
                return new Pair<K, Vector>(v, new Vector(s));
            }
        }
        return null;
    }
    /**
     * Used to pass parsed face list data on to the next processing steps.
     */
    public static class FaceListDescriptor {
        public final List<Object> faceLists;
        public final Map<Integer, Point> indexToPosition;
        public final List<Operator> symmetries;
        public final Matrix cellGramMatrix;
        public final Map<Pair<Operator, Integer>, Pair<Integer, Vector>>
                actions;

        public FaceListDescriptor(
                final List<Object> faceLists,
                final Map<Integer, Point> indexToPosition,
                final List<Operator> symmetries,
                final Map<Pair<Operator, Integer>, Pair<Integer, Vector>>
                        actions,
                final Matrix cellGramMatrix)
        {
            this.faceLists = faceLists;
            this.indexToPosition = indexToPosition;
            this.symmetries = symmetries;
            this.actions = actions;
            this.cellGramMatrix = cellGramMatrix;
        }

        public String toString() {
            return "FaceListDescriptor("
                    + faceLists + ", "
                    + indexToPosition + ", "
                    + symmetries + ", "
                    + cellGramMatrix + ")";
        }
    }
    /**
     * Constructs an instance.
     *
     * @param input the input stream.
     */
    public NetParser(final Reader input) {
        this(new BufferedReader(input));
    }

    /**
     * Parses the input stream as specified in the constructor and returns the
     * net specified by it.
     *
     * @return the periodic net constructed from the input.
     */
    public Net parseNet() {
        return parseNet(parseDataBlock());
    }
    /**
     * Utility method - takes a string and directly returns the net specified by it.
     *
     * @param s the specification string.
     * @return the net constructed from the input string.
     */
    public static PeriodicGraph stringToNet(final String s) {
        return new NetParser(new StringReader(s)).parseNet();
    }

}
